#pragma once
#include "MapObjectManager.h"
#include "BaseMapData.h"
class WorldBulder
{
public:
	WorldBulder();
	~WorldBulder();
	virtual void createCoints(MapObjectManager *& m) = 0;
	virtual void createMainCollectItems(MapObjectManager *& m) = 0;
	virtual void createBarriers(MapObjectManager *& m, sf::RenderWindow &renderWindows) = 0;
	std::vector<BaseMapData> injectData(std::string key);
	
	const std::string collectItemsName = "collect_items";
	const std::string mainItemsName = "main_items";
	const std::string barriersName = "barriers";
};

