#pragma once
#include "CollectItem.h"
#include "string.h"
#include "LargeCoin.h"
#include "SimpleCoin.h"
#include "LifeCollectItem.h"
#include "MainCollectItem.h"
#include "EarthBorderCollectItemDecorator.h"
#include "EarthTextureCollectItemDecorator.h"
#include "SpaceTextureCollectItemDecorator.h"
#include "SpaceBorderCollectItemDecorator.h"
class CollectItemFactory
{
	static CollectItemFactory *instance;
	std::map<std::string, CollectItem*> collectItemsMap;
	CollectItemFactory();
	CollectItem * CreateNewItem(std::string key, sf::Vector2f coord);
public:
	static CollectItemFactory *getInstance();
	~CollectItemFactory();
	CollectItem *CreateItem(std::string key, sf::Vector2f coord);
};

