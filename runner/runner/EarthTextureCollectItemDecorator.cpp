#include "EarthTextureCollectItemDecorator.h"
EarthTextureCollectItemDecorator::EarthTextureCollectItemDecorator(CollectItem * item) : CollectItemDecorator(item) {
	setShape();
}


EarthTextureCollectItemDecorator::~EarthTextureCollectItemDecorator()
{
}

void EarthTextureCollectItemDecorator::applyStyle()
{
	CollectItemDecorator::SetFillColor(sf::Color::Red);
}
