#pragma once
#include "CollectItem.h"

class CollectItemDecorator : public CollectItem
{
	CollectItem *item;
public:
	CollectItemDecorator(CollectItem *i);
	~CollectItemDecorator();
	std::string getName();
	void addPoints();
	void addLifePoins(Life playerLife);
	void applyStyle();

	
	void setShape();

};

