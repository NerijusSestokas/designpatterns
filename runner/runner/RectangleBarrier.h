#pragma once
#include "Barrier.h"
class RectangleBarrier : public Barrier
{
public:
	RectangleBarrier();
	~RectangleBarrier();

	int getDamagePoints();
	int getAwardPoints();
};

