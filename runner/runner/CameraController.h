#pragma once
#include "SFML\Graphics.hpp"
#include "CameraBehaviour.h"

class CameraController
{
public:
	CameraController();
	~CameraController();

	std::string GetBehaviourName();
	void SetCameraSize(float cameraSizeX, float cameraSizeY);
	void Move(float offsetX, float offsetY, sf::Vector2f playerPosition, float mapBoundaryX);
	void SetView(sf::RenderWindow & renderWindow);
	void ResetView();

	void SetBehavior(CameraBehaviour *behaviour);
	CameraBehaviour* GetBehaviour();

private:
	CameraBehaviour* _behaviour;
	sf::View _camera;
	float _camPosX;
	float _camPosY;
};

