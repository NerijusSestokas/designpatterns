#include "EarthWorldMapObjectFactory.h"



EarthWorldMapObjectFactory::EarthWorldMapObjectFactory()
{
}


EarthWorldMapObjectFactory::~EarthWorldMapObjectFactory()
{
}
Barrier * EarthWorldMapObjectFactory::createBarrier(sf::Vector2f coord, sf::RenderWindow &renderWindows)
{
	return  BarrierFactory::getInstance()->createBarrier("spikes", coord, renderWindows);
}

CollectItem * EarthWorldMapObjectFactory::createMainCollectItem(sf::Vector2f coord)
{
	return CollectItemFactory::getInstance()->CreateItem("main_item", coord);
}

CollectItem * EarthWorldMapObjectFactory::createCollectItem(sf::Vector2f coord)
{
	return CollectItemFactory::getInstance()->CreateItem("earth_item", coord);
}