#include "HighScore.h"
#include <fstream>
#include <gtest\gtest.h>

TEST(HIGHSCORETEST, GetPoints) {
	HighScore::getInstance()->setFilename("testScore.txt");
	HighScore::getInstance()->addToHighScore(5);
	std::vector<Score> scores = HighScore::getInstance()->getAllScores();
	EXPECT_EQ(1, scores.size());
	HighScore::getInstance()->addToHighScore(7);
	HighScore::getInstance()->addToHighScore(3);
	std::vector<Score> scoress = HighScore::getInstance()->getAllScores();
	EXPECT_EQ(3, scoress.size());
	std::ifstream aFile("testScore.txt");
	std::size_t lines_count = 0;
	std::string line;
	while (std::getline(aFile, line))
		++lines_count;
	EXPECT_EQ(3, lines_count);
}

TEST(HIGHSCORETEST, Max) {
	HighScore::getInstance()->addToHighScore(5);
	HighScore::getInstance()->addToHighScore(9);
	HighScore::getInstance()->addToHighScore(3);
	EXPECT_EQ(9, HighScore::getInstance()->getHighScore().getScore());
	remove("testScore.txt");
}