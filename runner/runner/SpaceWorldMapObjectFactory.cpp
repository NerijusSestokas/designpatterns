#include "SpaceWorldMapObjectFactory.h"



SpaceWorldMapObjectFactory::SpaceWorldMapObjectFactory()
{
}


SpaceWorldMapObjectFactory::~SpaceWorldMapObjectFactory()
{
}


Barrier * SpaceWorldMapObjectFactory::createBarrier(sf::Vector2f coord, sf::RenderWindow &renderWindows)
{
	return  BarrierFactory::getInstance()->createBarrier("cactus", coord, renderWindows);
}

CollectItem * SpaceWorldMapObjectFactory::createMainCollectItem(sf::Vector2f coord)
{
	return CollectItemFactory::getInstance()->CreateItem("main_item", coord);
}

CollectItem * SpaceWorldMapObjectFactory::createCollectItem(sf::Vector2f coord)
{
	return CollectItemFactory::getInstance()->CreateItem("space_item", coord);
}

