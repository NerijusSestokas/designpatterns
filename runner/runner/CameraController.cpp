#include "CameraController.h"



CameraController::CameraController() : _camPosX(0), _camPosY(0)
{
}


CameraController::~CameraController()
{
}

std::string CameraController::GetBehaviourName()
{
	return _behaviour->GetBehaviourName();
}

void CameraController::SetCameraSize(float cameraSizeX, float cameraSizeY)
{
	_camPosX = cameraSizeX;
	_camPosY = cameraSizeY;
	_camera.setSize({ cameraSizeX, cameraSizeY });
	_camera.setCenter({ cameraSizeX / 2, cameraSizeY * 0.5f });
}

void CameraController::Move(float offsetX, float offsetY, sf::Vector2f playerPosition, float mapBoundaryX)
{
	_behaviour->Move(_camera, offsetX, offsetY, playerPosition, mapBoundaryX);
}

void CameraController::SetView(sf::RenderWindow & renderWindow)
{
	renderWindow.setView(_camera);
}

void CameraController::ResetView()
{
	_camera.setCenter({ _camPosX / 2, _camPosY * 0.5f });
}

void CameraController::SetBehavior(CameraBehaviour *behaviour)
{
	_behaviour = behaviour;
}

CameraBehaviour* CameraController::GetBehaviour()
{
	return _behaviour;
}
