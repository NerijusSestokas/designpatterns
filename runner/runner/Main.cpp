#include "SFML\Graphics.hpp"
#include <iostream>
#include "Game.h"
#include <gtest\gtest.h>
#include "Points.h"
#include "PointsTest.h"
#include "HighScore.h"
#include "CollectItemFactory.h"
#include "BarrierFactory.h"
void runTests(int argc, char* argv[]);

Points* Points::instance = NULL;

HighScore* HighScore::instance = NULL;
CollectItemFactory * CollectItemFactory::instance = NULL;
BarrierFactory * BarrierFactory::instance = NULL;


int main(int argc, char* argv[])
{
	runTests(argc, argv);
	Game::start();
	return EXIT_SUCCESS;
}

void runTests(int argc, char* argv[])
{
	testing::InitGoogleTest(&argc, argv);
	RUN_ALL_TESTS();
}
