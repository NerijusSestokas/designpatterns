#include "Levels.h"

Levels::Levels(): level(0)
{
	levels.push_back(new GameLevel(1, "Earth"));
	levels.push_back(new GameLevel(2, "Earth"));
	levels.push_back(new GameLevelNull(3, "Earth"));
}

Levels::~Levels()
{
}


int Levels::ShowLevels(sf::RenderWindow & renderWindow)
{
	renderWindow.clear(sf::Color::Blue);
	for (unsigned int i = 0; i < levels.size(); i++)
	{
		sf::RectangleShape rec;
		if (levels.at(i)->isNull()) {
			rec.setFillColor(sf::Color::Magenta);
		}
		else {
			rec.setFillColor(sf::Color::Green);
		}
		rec.setPosition({ (2 * i + 1) * space.x, space.y });
		rec.setSize(size);
		renderWindow.draw(rec);
		buttons.push_back(rec);
		DrawText(std::to_string(levels.at(i)->getLevel()), (1.3f + 2*i ) * space.x, 1.2f * space.y, renderWindow);
	}

	unsigned short int lv = MouseClick(renderWindow);
	return lv;
}

void Levels::DrawText(std::string text, float posX, float posY, sf::RenderWindow & renderWindow)
{
	sf::Font font;
	if (!font.loadFromFile("fonts/Firecat.ttf"))
	{
		std::cout << "Unable to load font\n";
	}
	sf::Text tex;
	tex.setFont(font);
	tex.setCharacterSize(24);
	tex.setColor(sf::Color::Black);
	tex.setString(text);
	tex.setPosition(float(posX), float(posY));
	renderWindow.draw(tex);
}

void Levels::LoadLevel()
{
}

unsigned short int Levels::MouseClick(sf::RenderWindow & renderWindow)
{
	if (sf::Mouse::isButtonPressed(sf::Mouse::Button::Left)) {
		int x = sf::Mouse::getPosition(renderWindow).x;
		int y = sf::Mouse::getPosition(renderWindow).y;
		unsigned short int ind = 0;
		for (auto i = buttons.begin(); i != buttons.end(); ++i)
		{
			++ind;
			if (x > i->getPosition().x 
				&& x < i->getPosition().x + i->getSize().x
				&& y > i->getPosition().y
				&& y < i->getPosition().y + i->getSize().y)
			{
				std::cout << ind << "\n";
				if (levels.at(ind-1)->isNull()) {
					DrawText(levels.at(ind-1)->getName(), 125, 300, renderWindow);
					return 0;
				}
				return ind;
			}
		}
	}
	return 0;
}