#pragma once
#include "SFML\Graphics.hpp"
#include <fstream>
#include <sstream>
#include <time.h>

class GameMomento
{
	sf::Vector2f playerCoord;
	sf::Vector2f viewCoord;
	std::string identifier;
	float playerYCoord = 0;
public:
	GameMomento();
	~GameMomento();
	void setPlayerCoord(float a, float b);
	void setViewCoord(float a, float b);
	sf::Vector2f getPlayerCoord();
	sf::Vector2f getViewCoord();
	void setIdentifier();
	std::string getIndentifier();
};

