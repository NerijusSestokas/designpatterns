#pragma once
#include "MapObjectManager.h"

class VisibleGameObject
{
public:
	VisibleGameObject();
	virtual ~VisibleGameObject();

	virtual void Load(std::string fileName);
	virtual void Draw(sf::RenderWindow &renderWindow);
	virtual void Update(float elapsedTime, MapObjectManager mapObjects);

	virtual void SetPosition(float x, float y);
	virtual sf::Vector2f GetPosition() const;
	virtual float GetHeight() const;
	virtual float GetWidth() const;

	virtual sf::Rect<float> GetBounding() const;
	virtual bool IsLoaded() const;

protected:
	sf::Sprite &GetSprite();

	std::string GetFileName();

	void FlipSpriteLeft();
	void FlipSpriteRight();
	void SetScale(float x, float y);

private:
	sf::Vector2f scale;
	bool _isLoaded;
	sf::Sprite _sprite;
	sf::Texture _image;
	std::string _fileName;
};

