#pragma once

class Points
{
private:
	int points = 0;
	static Points *instance;
	Points();
	
public:
	~Points();
	void addPoints(int points);
	void substractPoints(int points);
	void nulifyPoints();
	int getPoints();
	void setPoints(int points);
	static Points *getInstance();
};

