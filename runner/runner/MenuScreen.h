#pragma once
#include "SFML\Graphics.hpp"
#include <iostream>
#include "MenuItem.h"

class MenuScreen
{
	const int Xpos = 10;
	const int Space = 50;
	const int ButtonWidth = 200;
	const int ButtonHeight = 24;

public:
	/*
	@return 0 nothing
	1 close main window
	2 play game
	3 high score
	*/
	unsigned short int ShowMenu(sf::RenderWindow & renderWindow);

private:
	std::vector<MenuItem> menuItems;
	MenuItem::MenuState menuStates;		// Gal nereiks

	unsigned short int MenuLoop(sf::RenderWindow & renderWindow);
	void MouseClick(sf::RenderWindow & renderWindow);
	void DrawText(std::string text, int posX, int posY, sf::RenderWindow & renderWindow);
};

