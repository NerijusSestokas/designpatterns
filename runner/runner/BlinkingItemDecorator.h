#pragma once
#include "CollectItemDecorator.h"

class BlinkinkItemDecorator : public CollectItemDecorator
{
public:
	BlinkinkItemDecorator(CollectItem *item);
	~BlinkinkItemDecorator();
	
	void display(sf::Vector2f vector, sf::RenderWindow &window);

private:
	sf::Time time;

};

