#pragma once
#include "string"
#include "Points.h"
#include "Life.h"
#include "MapObject.h"

class CollectItem : public MapObject
{
private:
	sf::CircleShape _circle;
public:
	CollectItem();
	~CollectItem();
	
	virtual std::string getName() = 0;
	virtual void addPoints() = 0;
	virtual void addLifePoins(Life playerLife) = 0;
};

