#pragma once
#include "AbstractLogger.h"
#include "DisplayAdapter.h"
#include "Display.h"

class ConsoleLogger : public AbstractLoger
{
public:
	ConsoleLogger(int level);
	void logMessage(std::string message);
};

