#include "Score.h"
Score::Score(std::string date, int score)
{
	this->date = date;
	this->score = score;
}

Score::~Score()
{
}

int Score::getScore()
{
	return this->score;
}

void Score::setScore(int score)
{
	this->score = score;
}

std::string Score::getDate()
{
	return this->date;
}

void Score::setDate(std::string date)
{
	this->date = date;
}

std::string Score::toString()
{
	std::stringstream stream;
	stream << this->date << ", " << this->score << "\n";
	return stream.str();
}
