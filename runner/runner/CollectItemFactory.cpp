#include "CollectItemFactory.h"

CollectItemFactory::CollectItemFactory()
{
}

CollectItemFactory * CollectItemFactory::getInstance()
{
	if (!instance) {
		instance = new CollectItemFactory();
	}
	return instance;
}

CollectItemFactory::~CollectItemFactory()
{
}

CollectItem *CollectItemFactory::CreateItem(std::string key, sf::Vector2f coord)
{
	CollectItem *item = NULL;

	item = CreateNewItem(key, coord);
	

	return item;
}

CollectItem *CollectItemFactory::CreateNewItem(std::string key, sf::Vector2f coord) {
	CollectItem *item;

	if (key == "space_item")
	{
		item = new SpaceBorderCollectItemDecorator(new SpaceTextureCollectItemDecorator(new SimpleCoin(coord, 10.0f)));
	}
	else if (key == "life_item")
	{
		item = new LifeCollectItem(coord);
	}
	else if (key == "main_item")
	{
		item = new SimpleCoin(coord, 20.0f);
	}
	else {
		item = new EarthBorderCollectItemDecorator(new EarthTextureCollectItemDecorator(new SimpleCoin(coord, 20.0f)));
	}
	return item;
}
