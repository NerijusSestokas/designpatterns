#include "SpikesBarrier.h"


SpikesBarrier::SpikesBarrier(sf::Vector2f v)
{
	SetupRect(v.x, v.y, {20, 30 }, sf::Color::Black);
}

SpikesBarrier::~SpikesBarrier()
{
}

void SpikesBarrier::addAwardsPoints()
{
	Points::getInstance()->addPoints(50);
}

void SpikesBarrier::substractDemagePoints()
{
	Points::getInstance()->substractPoints(100);
}

void SpikesBarrier::subsctractLifeDemagePoints(Life playerLife)
{
	playerLife.subtractHearts(1);
}
