#include "MenuItem.h"

MenuItem::MenuItem()
{
}

void MenuItem::SetItem(int posX, int posY, MenuState act)
{
	rect.left = posX;
	rect.top = posY;
	menuState = act;
}

MenuItem::MenuState MenuItem::GetState()
{
	return menuState;
}

int MenuItem::GetLeft()
{
	return rect.left;
}

int MenuItem::GetTop()
{
	return rect.top;
}
