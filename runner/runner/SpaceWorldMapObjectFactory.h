#pragma once
#include "WorldMapObjectFactory.h"

class SpaceWorldMapObjectFactory
{
public:
	SpaceWorldMapObjectFactory();
	~SpaceWorldMapObjectFactory();
	Barrier * createBarrier(sf::Vector2f coord, sf::RenderWindow &renderWindows);
	CollectItem * createMainCollectItem(sf::Vector2f coord);
	CollectItem * createCollectItem(sf::Vector2f coord);

};

