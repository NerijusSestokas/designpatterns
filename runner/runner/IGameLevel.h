#pragma once
#include <string>
class IGameLevel
{
public:
	virtual ~IGameLevel() {};
	virtual int getLevel() = 0;
	virtual std::string getName() = 0;
	virtual std::string getFilesDir() = 0;
	virtual std::string getWorld() = 0;
	virtual bool isNull() = 0;
};

