#include "EarthBorderCollectItemDecorator.h"
#include <iostream>
EarthBorderCollectItemDecorator::EarthBorderCollectItemDecorator(CollectItem * item) : CollectItemDecorator(item) {
	setShape();
}


EarthBorderCollectItemDecorator::~EarthBorderCollectItemDecorator()
{
}

void EarthBorderCollectItemDecorator::applyStyle()
{
	CollectItemDecorator::SetOutline(20, sf::Color::Black);
}
