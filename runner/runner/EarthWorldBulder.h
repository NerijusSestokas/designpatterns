#pragma once
#include "WorldBulder.h"
#include "EarthWorldMapObjectFactory.h"

class EarthWorldBulder : WorldBulder
{
	std::string path = "Earth";
	std::string getFilename();
	EarthWorldMapObjectFactory factory;
	int level = 1;
public:
	EarthWorldBulder();
	EarthWorldBulder(int lvl);
	~EarthWorldBulder();
	void createCoints(MapObjectManager *&m);
	void createMainCollectItems(MapObjectManager*& m);
	void createBarriers(MapObjectManager*& m, sf::RenderWindow &renderWindows);
};

