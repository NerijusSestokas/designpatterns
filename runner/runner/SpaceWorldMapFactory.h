#pragma once
#include "WorldMapFactory.h"

class SpaceWorldMapFactory
{
public:
	SpaceWorldMapFactory();
	~SpaceWorldMapFactory();
	Barrier * createBarrier(sf::Vector2f coord);
	CollectItem * createBlinkingCoin(sf::Vector2f coord);
	CollectItem * createCoin(sf::Vector2f coord);
};

