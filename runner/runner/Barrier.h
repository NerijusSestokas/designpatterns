#pragma once
#include "Life.h"
#include "MapObject.h"

class Barrier : public MapObject
{
public:
	Barrier();
	~Barrier();
	virtual void addAwardsPoints() = 0;
	virtual void substractDemagePoints() = 0;
	virtual void subsctractLifeDemagePoints(Life life) = 0;
};

