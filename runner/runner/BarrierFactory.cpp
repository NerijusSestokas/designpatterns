#include "BarrierFactory.h"



BarrierFactory::BarrierFactory()
{
}


BarrierFactory::~BarrierFactory()
{
}

BarrierFactory * BarrierFactory::getInstance()
{
	if (!instance) {
		instance = new BarrierFactory();
	}
	return instance;
}

Barrier *BarrierFactory::createBarrier(std::string key, sf::Vector2f coord, sf::RenderWindow &renderWindows)
{
	Barrier *barrier;

		if (key == "spikes") {
			barrier = new SpikesBarrier(coord);
		}
		else {
			barrier = new CactusBarrier(coord);
		}

	
	return barrier;
}
