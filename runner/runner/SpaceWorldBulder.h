#pragma once
#include  "WorldBulder.h"
#include "SpaceWorldMapObjectFactory.h"

class SpaceWorldBulder : WorldBulder
{
	std::string path = "Space";
	SpaceWorldMapObjectFactory factory;
	int level;
	std::string getFilename();
public:
	SpaceWorldBulder();
	SpaceWorldBulder(int lvl);
	~SpaceWorldBulder();

	void createCoints(MapObjectManager *&m);
	void createMainCollectItems(MapObjectManager*& m);
	void createBarriers(MapObjectManager*& m, sf::RenderWindow &renderWindows);
};

