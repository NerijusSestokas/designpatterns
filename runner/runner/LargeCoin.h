#pragma once
#include "CollectItem.h"
#include "MapObject.h"

class LargeCoin : public CollectItem
{
public:
	LargeCoin(sf::Vector2f coord);
	~LargeCoin();

	std::string getName();
	void addPoints();
	void addLifePoins(Life playerLife);
};

