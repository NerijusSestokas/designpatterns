#include "MovingCam.h"



MovingCam::MovingCam()
{
}


MovingCam::~MovingCam()
{
}

std::string MovingCam::GetBehaviourName()
{
	return Name;
}

void MovingCam::Move(sf::View & cam, float offsetX, float offsetY, sf::Vector2f playerPosition, float mapBoundaryX)
{
	if (cam.getCenter().x < mapBoundaryX - (cam.getSize().x / 2))
	{
		cam.move(offsetX, offsetY);
	}
}