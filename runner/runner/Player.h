#pragma once
#include <assert.h>
#include "VisibleGameObject.h"
#include "MapObjectManager.h"
#include "CameraController.h"
#include "FollowingCam.h"
#include "MovingCam.h"

class Player:
	public VisibleGameObject
{
public:
	Player();
	~Player();

	void Update(float elapsedTime);
	void Draw(sf::RenderWindow &renderWindow);

	float GetSpeed() const;

	void SetMap(MapObjectManager* map);

	sf::Vector2f GetPlayerPosition();

private:
	sf::Clock clkJump;
	sf::Clock clkAnim;
	float clkElapsed;

	bool _jumpBool;
	bool _floor;
	float _runSpeed; // - left + right
	float _maxSpeed;
	float _jump;	// - crouch + jump
	float _maxJump;
	float _timeEx;
	
	CameraController cam;

	sf::IntRect textureRect;
	MapObjectManager* map;

	void Move();
	void texturePosition(unsigned short int anim);	// 0 - idle, 1 - run, 2 - jump
	void CheckFloor();
	void PlayTexture();
	void CheckCoins();
};