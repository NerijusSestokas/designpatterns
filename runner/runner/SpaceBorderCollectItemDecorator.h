#pragma once
#include "CollectItemDecorator.h"

class SpaceBorderCollectItemDecorator : public CollectItemDecorator
{
public:
	SpaceBorderCollectItemDecorator(CollectItem *item);
	~SpaceBorderCollectItemDecorator();
	void applyStyle();
};

