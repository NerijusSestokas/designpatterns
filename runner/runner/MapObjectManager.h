#pragma once
#include "MapObject.h"
#include "SFML\Graphics.hpp"
#include <iostream>

class MapObjectManager
{
public:
	MapObjectManager();
	~MapObjectManager();

	void DrawAll(sf::RenderWindow &renderWindow);

	void Add(MapObject *mapObject);
	int GetObjectCount() const;
	MapObject* Get(int ind);

	void SetLevelBoundaryX(float x);
	float GetLevelBoundaryX();

private:
	std::vector <MapObject*> _mapObjects;
	float levelBoundaryX;
};
