#pragma once
#include "MapObjectManager.h"
#include "EarthWorldBulder.h"
#include "SpaceWorldBulder.h"

class MapDirector
{
public:
	MapDirector();
	~MapDirector();
	MapObjectManager * buildEarthWorld(int lvl, sf::RenderWindow &renderWindows);
	MapObjectManager * buildSpaceWorld(int lvl, sf::RenderWindow &renderWindows);
};

