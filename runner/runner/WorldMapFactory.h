#pragma once
#include "BarrierFactory.h"
#include "CollectItemFactory.h"

class WorldMapFactory
{
public:
	WorldMapFactory();
	~WorldMapFactory();
	virtual Barrier * createBarrier(sf::Vector2f coord) = 0;
	virtual Barrier * createBlinkingCoin(sf::Vector2f coord) = 0;
	virtual CollectItem * createCoin(sf::Vector2f coord) = 0;

};

