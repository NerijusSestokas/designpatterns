#include "LargeCoin.h"

LargeCoin::LargeCoin(sf::Vector2f coord)
{
	SetupCircle(coord.x, coord.y, 40.0f, sf::Color::Yellow);
}


LargeCoin::~LargeCoin()
{
}

std::string LargeCoin::getName()
{
	return "Large Coin";
}

void LargeCoin::addPoints() {
	Points::getInstance()->addPoints(20);
}

void LargeCoin::addLifePoins(Life playerLife)
{
	playerLife.addHearts(0);
}

std::string getName() {
	return "Large Coin";
}
