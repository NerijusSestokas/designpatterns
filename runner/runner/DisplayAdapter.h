#pragma once
#include "Display.h"
#include "adaptee.h"

class DisplayAdapter : public Display, private adaptee
{
public:
	virtual void print(std::string text);
};

