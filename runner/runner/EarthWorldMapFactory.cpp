#include "EarthWorldMapFactory.h"



EarthWorldMapFactory::EarthWorldMapFactory()
{
}


EarthWorldMapFactory::~EarthWorldMapFactory()
{
}
Barrier * EarthWorldMapFactory::createBarrier(sf::Vector2f coord)
{
	return  BarrierFactory::getInstance()->createBarrier("rectangle", coord);
}

CollectItem * EarthWorldMapFactory::createBlinkingCoin(sf::Vector2f coord)
{
	return CollectItemFactory::getInstance()->CreateItem("big_blinking", coord);
}

CollectItem * EarthWorldMapFactory::createCoin(sf::Vector2f coord)
{
	return CollectItemFactory::getInstance()->CreateItem("big", coord);
}