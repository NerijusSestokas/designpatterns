#pragma once
#include "SFML\Graphics.hpp"

class CameraBehaviour
{
public:
	virtual std::string GetBehaviourName() = 0;
	virtual void Move(sf::View & cam, float offsetX, float offsetY, sf::Vector2f playerPosition, float mapBoundaryX) = 0;
};

