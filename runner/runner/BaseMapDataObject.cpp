#include "BaseMapData.h"



BaseMapData::BaseMapData()
{
}


BaseMapData::~BaseMapData()
{
}

BaseMapData::BaseMapData(std::string key, float x, float y)
{
	setCoord(x, y);
	setKey(key);
}

void BaseMapData::setCoord(float x, float y)
{
	coord = {x, y};
}

void BaseMapData::setCoord(sf::Vector2f v)
{
	coord = v;
}

void BaseMapData::setKey(std::string key)
{
	this->key = key;
}

std::string BaseMapData::getKey()
{
	return key;
}

sf::Vector2f BaseMapData::getCoord()
{
	return coord;
}
