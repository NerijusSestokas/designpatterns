#pragma once
class Life
{
private:
	bool isAlive;
	unsigned short int hearts;
public:
	Life();
	~Life();
	unsigned short int getHearts();
	bool checkIsAlive();
	void addHearts(unsigned short int hearts);
	void subtractHearts(unsigned short int hearts);
};

