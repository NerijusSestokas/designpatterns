#pragma once
#include <string>

class Display
{
public:
	virtual void print(std::string text) = 0;
};

