#include "VisibleGameObject.h"


VisibleGameObject::VisibleGameObject() : _isLoaded(false), scale(1.0f,1.0f)
{
}

VisibleGameObject::~VisibleGameObject()
{
}

void VisibleGameObject::Load(std::string fileName)
{
	if (_image.loadFromFile(fileName) == false)
	{
		_fileName = "";
		_isLoaded = false;
	}
	else 
	{
		_sprite.setTexture(_image);
		_fileName = fileName;
		_isLoaded = true;
		_sprite.setScale(scale);
	}
}

void VisibleGameObject::Draw(sf::RenderWindow & renderWindow)
{
	if (_isLoaded)
		renderWindow.draw(_sprite);
}

void VisibleGameObject::Update(float elapsedTime, MapObjectManager mapObjects)
{
}

void VisibleGameObject::SetPosition(float x, float y)
{
	if (_isLoaded)
	{
		_sprite.setPosition(x, y);
	}
}

sf::Vector2f VisibleGameObject::GetPosition() const
{
	if (_isLoaded)
	{
		return _sprite.getPosition();
	}
	return sf::Vector2f();
}

float VisibleGameObject::GetHeight() const
{
	return _sprite.getLocalBounds().height;
}

float VisibleGameObject::GetWidth() const
{
	return _sprite.getLocalBounds().width;
}

sf::Rect<float> VisibleGameObject::GetBounding() const
{
	return _sprite.getGlobalBounds();
}

bool VisibleGameObject::IsLoaded() const
{
	return _isLoaded;
}

sf::Sprite & VisibleGameObject::GetSprite()
{
	return _sprite;
}

std::string VisibleGameObject::GetFileName()
{
	return _fileName;
}

void VisibleGameObject::FlipSpriteLeft()
{
	_sprite.setOrigin({ _sprite.getLocalBounds().width, 0 });
	_sprite.setScale({ -scale.x, scale.y });
}

void VisibleGameObject::FlipSpriteRight()
{
	_sprite.setOrigin({ _sprite.getLocalBounds().left, 0 });
	_sprite.setScale({ scale.x, scale.y });
}

void VisibleGameObject::SetScale(float x, float y)
{
	scale.x = x;
	scale.y = y;
	_sprite.setScale(scale);
}

