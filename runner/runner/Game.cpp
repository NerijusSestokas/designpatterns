#include "Game.h"

sf::RenderWindow Game::mainWindow;
Game::GameState Game::gameState = Game::GameState::Uninitialized;
sf::Event Game::currentEvent;
MapObjectManager* Game::mapObjectManager = new MapObjectManager();
int level = 1;
VisibleGameObject Game::backGround;
GameMomentoCareTaker Game::momentoContainer;
float Game::elapsedSeconds = 0;
bool Game::exit = false;
IGameState* Game::state = new GameStateShowingStartMenu();

Player Game::player;

void Game::start()
{
	setState(new GameStateShowingStartMenu());

	mainWindow.create(sf::VideoMode(SCREEN_WIDTH, SCREEN_HEIGHT, 32), "Runner");

	gameState = GameState::ShowingStartMenu;

	player.SetPosition((SCREEN_WIDTH * 0.2f), SCREEN_HEIGHT*0.5f);

	backGround.Load("Background/background.jpg");

	loadMap();

	AbstractLoger *log = getChainLoggers();

	log->handle(log->_info, "paleistas zaidimas");

	while (!exit)
	{
		gameLoop();
	}

	log->handle(log->_error, "zaidimas baigtas");
}

void Game::gameLoop()
{
	sf::Clock clk;
	elapsedSeconds = clk.getElapsedTime().asSeconds();
	mainWindow.pollEvent(currentEvent);
	state->doAction();
	mainWindow.display();
	handleEvents();
	clk.restart().asSeconds();
}

void Game::handleEvents()
{
	mainWindow.pollEvent(currentEvent);
	if (sf::Keyboard::isKeyPressed(sf::Keyboard::Escape))
	{
		backToMainScreen();
	}

	if (sf::Keyboard::isKeyPressed(sf::Keyboard::P)) {
		addToMomentoList();
		backToMainScreen();
	}

	if (currentEvent.type == sf::Event::Closed)
	{
		setState(new GameStateExiting());
	}
}

void Game::backToMainScreen()
{
	setState(new GameStateShowingStartMenu());
	sf::View view = mainWindow.getView();
	view.setCenter(SCREEN_WIDTH / 2, SCREEN_HEIGHT / 2);
	mainWindow.setView(view);
	player.SetPosition((SCREEN_WIDTH * 0.2f), SCREEN_HEIGHT*0.5f);
}

void Game::play() {
	mainWindow.clear(sf::Color::Green);
	backGround.SetPosition(0, -150);
	backGround.Draw(mainWindow);
	mapObjectManager->DrawAll(mainWindow);
	player.Update(elapsedSeconds*200);
	player.Draw(mainWindow);
}

void Game::exitGame() {
	exit = true;
	mainWindow.close();
}

void Game::showMenu()
{
	MenuScreen menu;
	unsigned short int re = menu.ShowMenu(mainWindow);
	switch (re)
	{
		case 1:
		{
			setState(new GameStateExiting());
			break;
		}
		case 2:
		{
			setState(new GameStatePlaying());
			break;
		}
		case 4:
		{
			setState(new GameStateShowingLevels());
			break;
		}
		case 5:
		{
			setState(new GameStateShowingMomento());
			break;
		}
	}
}

void Game::loadMap()
{
	MapDirector mapdir;
	mapObjectManager = mapdir.buildEarthWorld(level, mainWindow);
	MapObject* object = new MapObject();
	object->SetupRect(0.3f, SCREEN_HEIGHT * 0.8f, { backGround.GetWidth(), 10 }, sf::Color::Red);
	mapObjectManager->Add(object);

	mapObjectManager->SetLevelBoundaryX(backGround.GetWidth());
	player.SetMap(mapObjectManager);
}

void Game::showLevels()
{
	Levels lev;
	int value = lev.ShowLevels(mainWindow);
	if (value > 0) {
		level = value;
		loadMap();
		setState(new GameStatePlaying());
	}
}

void Game::showMomento()
{
	if (momentoContainer.getList().size() > 0) {
		GameMomento momento = momentoContainer.getMomento();
		player.SetPosition(momento.getPlayerCoord().x, momento.getPlayerCoord().y);

		setState(new GameStateShowingStartMenu());
		sf::View view = mainWindow.getView();
		view.setCenter(momento.getViewCoord().x, momento.getViewCoord().y);
		mainWindow.setView(view);
	}
	setState(new GameStatePlaying());

}

AbstractLoger* Game::getChainLoggers()
{
	AbstractLoger *info = new ConsoleLogger(1);
	AbstractLoger *error = new ErrorLogger(2);

	info->setNext(error);
	return info;
}

void Game::addToMomentoList()
{
	sf::View view = mainWindow.getView();
	GameMomento momento;

	momento.setPlayerCoord(player.GetPosition().x, player.GetPosition().y);
	momento.setViewCoord(view.getCenter().x, view.getCenter().y);	
	std::cout << "player " << momento.getPlayerCoord().x << " : " << momento.getPlayerCoord().y << "\n";

	std::cout << "view " << momento.getViewCoord().x << " : " << momento.getViewCoord().y << "\n";
	momentoContainer.addGameMomento(momento);
}

GameMomento Game::loadFromMomento(int index)
{
	return momentoContainer.getMomento(index);
}


void Game::setState(IGameState *staste) {
	state = staste;
}

IGameState* Game::getState() {
	return state;
}

