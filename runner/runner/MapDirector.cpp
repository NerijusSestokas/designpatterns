#include "MapDirector.h"



MapDirector::MapDirector()
{
}


MapDirector::~MapDirector()
{
}

MapObjectManager * MapDirector::buildEarthWorld(int lvl, sf::RenderWindow &renderWindows)
{
	MapObjectManager* manager = new MapObjectManager();
	EarthWorldBulder bulder = EarthWorldBulder(lvl);

	bulder.createCoints(manager);
	bulder.createMainCollectItems(manager);
	bulder.createBarriers(manager, renderWindows);

	return manager;
}

MapObjectManager *  MapDirector::buildSpaceWorld(int lvl, sf::RenderWindow &renderWindows)
{
	MapObjectManager* manager = new MapObjectManager();
	SpaceWorldBulder bulder = SpaceWorldBulder(lvl);

	bulder.createCoints(manager);
	bulder.createMainCollectItems(manager);
	bulder.createBarriers(manager, renderWindows);

	return manager;
}
