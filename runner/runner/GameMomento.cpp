#include "GameMomento.h"



GameMomento::GameMomento()
{
}


GameMomento::~GameMomento()
{
}

void GameMomento::setPlayerCoord(float x, float y)
{
	playerCoord.x = x;
	playerCoord.y = y;
}

void GameMomento::setViewCoord(float x, float y)
{
	viewCoord.x = x;
	viewCoord.y = y;
}

sf::Vector2f GameMomento::getPlayerCoord()
{
	return playerCoord;
}

sf::Vector2f GameMomento::getViewCoord()
{
	return viewCoord;
}

void GameMomento::setIdentifier()
{
	time_t rawtime = time(NULL);
	tm timeinfo;
	errno_t result = localtime_s(&timeinfo, &rawtime);

	std::stringstream dateStream;
	dateStream << timeinfo.tm_year + 1900 << "-" << timeinfo.tm_mon + 1 << "-" << timeinfo.tm_mday << " "
		<< timeinfo.tm_hour << ":" << timeinfo.tm_min;
	std::string date = dateStream.str();
}

std::string GameMomento::getIndentifier()
{
	return identifier;
}
