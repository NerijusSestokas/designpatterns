#pragma once
#include <string>
#include "Barrier.h"
#include "CactusBarrier.h"
#include "SpikesBarrier.h"
#include "SFML\Graphics.hpp"

class BarrierFactory
{
	static BarrierFactory *instance;
	BarrierFactory();
	std::map<std::string, Barrier*> barriersItemsMap;
public:
	~BarrierFactory();
	static BarrierFactory * getInstance();
	Barrier *createBarrier(std::string key, sf::Vector2f coord, sf::RenderWindow &renderWindows);
};

