#pragma once
#include "GameMomento.h"

class GameMomentoCareTaker
{
	std::vector<GameMomento> momentoList;
public:
	GameMomentoCareTaker();
	~GameMomentoCareTaker();
	void addGameMomento(GameMomento momento);
	GameMomento getMomento(int index);
	GameMomento getMomento();
	std::vector<GameMomento> getList();
};

