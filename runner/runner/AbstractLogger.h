#pragma once
#include <iostream>
#include <string>

class AbstractLoger {
public:
	int _info = 1;
	int _error = 2;
	AbstractLoger();
	void setNext(AbstractLoger *n);
	void add(AbstractLoger *n);
	void handle(int level, std::string message);
	virtual void logMessage(std::string message);
protected:
	int _level;
private:
	AbstractLoger * _next;
};
