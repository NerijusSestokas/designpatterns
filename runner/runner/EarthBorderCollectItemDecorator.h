#pragma once
#include "CollectItemDecorator.h"
class EarthBorderCollectItemDecorator : public CollectItemDecorator
{
public:
	EarthBorderCollectItemDecorator(CollectItem * item);
	~EarthBorderCollectItemDecorator();
	void applyStyle();
};

