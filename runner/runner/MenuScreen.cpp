#include "MenuScreen.h"

unsigned short int MenuScreen::ShowMenu(sf::RenderWindow & renderWindow)
{
	int Y = renderWindow.getSize().y / 2;
	int X = renderWindow.getSize().x / 2 - 100;

	renderWindow.clear(sf::Color::Blue);
	
	DrawText("  Start", X, Y - Space, renderWindow);
	DrawText("High score", X, Y, renderWindow);
	DrawText("  Levels", X, Y + Space, renderWindow);
	DrawText("   Exit", X, Y + 3 * Space, renderWindow);
	DrawText("  Resume", X, Y + 2 * Space, renderWindow);
	MenuItem itm;
	itm.SetItem(X, Y - Space, itm.Play);
	menuItems.push_back(itm);
	itm.SetItem(X, Y, itm.HighScore);
	menuItems.push_back(itm);
	itm.SetItem(X, Y + Space, itm.Levels);
	menuItems.push_back(itm);
	itm.SetItem(X, Y + 2 * Space, itm.Momento);
	menuItems.push_back(itm);
	itm.SetItem(X, Y + 3 * Space, itm.Exit);
	menuItems.push_back(itm);

	return MenuLoop(renderWindow);
}

unsigned short int MenuScreen::MenuLoop(sf::RenderWindow & renderWindow)
{
	MouseClick(renderWindow);
	switch (menuStates)
	{
		case MenuItem::Play: 
		{
			menuStates = MenuItem::Nothing;
			return 2;
			break;
		}
		case MenuItem::HighScore:
		{
			menuStates = MenuItem::Nothing;
			return 3;
			break;
		}
		case MenuItem::Levels:
		{
			menuStates = MenuItem::Levels;
			return 4;
			break;
		}
		case MenuItem::Momento:
		{
			menuStates = MenuItem::Momento;
			return 5;
			break;
		}
		case MenuItem::Exit:
		{
			renderWindow.close();
			return 1;
		}
		default:
			return 0;
	}
}

void MenuScreen::MouseClick(sf::RenderWindow & renderWindow)
{
	if (sf::Mouse::isButtonPressed(sf::Mouse::Button::Left)) {
		int x = sf::Mouse::getPosition(renderWindow).x;
		int y = sf::Mouse::getPosition(renderWindow).y;
		menuItems.begin();
		for (unsigned int i = 0; i < menuItems.size(); i++)
		{
			if (x > menuItems.at(i).GetLeft()
				&& x < menuItems.at(i).GetLeft() + ButtonWidth
				&& y > menuItems.at(i).GetTop()
				&& y < menuItems.at(i).GetTop() + ButtonHeight)
			{
				menuStates = menuItems.at(i).GetState();
				break;
			}
		}
	}
}

void MenuScreen::DrawText(std::string text, int posX, int posY, sf::RenderWindow & renderWindow)
{
	sf::Font font;
	if (!font.loadFromFile("fonts/Firecat.ttf"))
	{
		std::cout << "Unable to load font\n";
	}
	sf::Text tex;
	tex.setFont(font);
	tex.setCharacterSize(24);
	tex.setColor(sf::Color::Black);
	tex.setString(text);
	tex.setPosition(float(posX), float(posY));
	renderWindow.draw(tex);
}
