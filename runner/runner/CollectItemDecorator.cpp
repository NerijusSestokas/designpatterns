#include "CollectItemDecorator.h"

CollectItemDecorator::CollectItemDecorator(CollectItem * i)
{
	item = i;
}

CollectItemDecorator::~CollectItemDecorator()
{
}

std::string CollectItemDecorator::getName()
{
	return item->getName();
}

void CollectItemDecorator::addPoints()
{
	item->addPoints();
}

void CollectItemDecorator::addLifePoins(Life playerLife)
{
	item->addLifePoins(playerLife);
}

void CollectItemDecorator::applyStyle()
{
	item->applyStyle();
}

void CollectItemDecorator::setShape()
{
//	SetupCircle(item->getShape().getPosition().x, item->getShape().getPosition().y, item->getShape().getRadius(), item->getShape().getFillColor());
	SetupCircle(item->getShape().getPosition().x, item->getShape().getPosition().y, item->getShape().getRadius(), sf::Color::Green);
}/*

void CollectItemDecorator::SetFillColor(sf::Color color)
{
	item->SetFillColor(color);
}

void CollectItemDecorator::SetOutline(int thickness, sf::Color Color)
{
	item->SetOutline(thickness, Color);
}

void CollectItemDecorator::SetTexture(sf::Texture texture)
{
	item->SetTexture(texture);
}*/