#pragma once
#include "IGameLevel.h"

class GameLevelNull : public IGameLevel
{
protected:
	std::string world;
	int level;
	std::string filename;
public:
	GameLevelNull();
	~GameLevelNull();
	GameLevelNull(int lvl, std::string world);
	std::string getName();
	std::string getFilesDir();
	bool isNull();

	int getLevel();
	std::string getWorld();
};

