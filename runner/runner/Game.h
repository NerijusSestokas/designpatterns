#pragma once
#include "SFML\Graphics.hpp"
#include "MenuScreen.h"
#include "Player.h"
#include "Levels.h"
#include "EarthWorldMapObjectFactory.h"
#include "SpaceWorldMapObjectFactory.h"
#include "MapDirector.h"

#include "AbstractLogger.h"
#include "ConsoleLogger.h"
#include "ErrorLogger.h"
#include "GameMomentoCareTaker.h"
#include "GameLevelNull.h"

class Game
{
private :	
	static class IGameState *state;
	static GameMomentoCareTaker momentoContainer;

public:
	const static int SCREEN_WIDTH = 800;
	const static int SCREEN_HEIGHT = 600;
	static void start();

	class IGameState * getState();
	static void setState(IGameState * state);
	static void gameLoop();
	static void handleEvents();
	static void backToMainScreen();
	static void play();
	static void exitGame();
	static void showMenu();
	static void loadMap();
	static void showLevels();
	static void showMomento();
	static AbstractLoger* getChainLoggers();
	static void addToMomentoList();

private:
	enum GameState {
		Uninitialized, ShowingStartMenu, ShowingLevels, Playing, Exiting
	};

	static sf::RenderWindow mainWindow;
	static GameState gameState;
	static GameMomento loadFromMomento(int index);
	static float elapsedSeconds;
	static sf::Event currentEvent;

	static MapObjectManager* mapObjectManager;
	static Player player;
	static bool exit;
	static VisibleGameObject backGround;
};


class IGameState
{
public:
	virtual void doAction() = 0;
	virtual std::string getKey() = 0;
};

class GameStatePlaying : public IGameState
{
public:
	GameStatePlaying() {}
	~GameStatePlaying() {}
	void doAction() {
		Game::play();
	}

	std::string getKey() {
		return "Playing";
	}
};

class GameStateUninitialized : public IGameState
{
public:
	GameStateUninitialized() {}
	~GameStateUninitialized() {}
	void doAction() {
		Game::setState(new GameStateUninitialized());
	}

	std::string getKey() {
		return "Playing";
	}
};

class GameStateShowingStartMenu : public IGameState
{
public:
	GameStateShowingStartMenu() {}
	~GameStateShowingStartMenu() {}
	void doAction() {
		Game::showMenu();
	}

	std::string getKey() {
		return "ShowingStartMenu";
	}
};

class GameStateShowingLevels : public IGameState
{
public:
	GameStateShowingLevels() {}
	~GameStateShowingLevels() {}
	void doAction() {
		Game::showLevels();
	}

	std::string getKey() {
		return "ShowingLevels";
	}
};

class GameStateShowingMomento : public IGameState
{
public:
	GameStateShowingMomento() {}
	~GameStateShowingMomento() {}
	void doAction() {
		Game::showMomento();
	}

	std::string getKey() {
		return "ShowingMomento";
	}
};


class GameStateExiting : public IGameState
{
public:
	GameStateExiting() {}
	~GameStateExiting() {}
	void doAction() {
		Game::exitGame();
	}

	std::string getKey() {
		return "Exiting";
	}
};
