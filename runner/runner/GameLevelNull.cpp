#include "GameLevelNull.h"



GameLevelNull::GameLevelNull()
{
}


GameLevelNull::~GameLevelNull()
{
}

GameLevelNull::GameLevelNull(int lvl, std::string world)
{
	this->level = lvl;
	this->world = world;
}



std::string GameLevelNull::getName()
{
	return "Level is not available yet";
}

std::string GameLevelNull::getFilesDir()
{
	return "Level is not available yet";
}

bool GameLevelNull::isNull()
{
	return true;
}

int GameLevelNull::getLevel()
{
	return this->level;
}

std::string GameLevelNull::getWorld()
{
	return this->world;
}
