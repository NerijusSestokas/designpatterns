#include "MapObject.h"


MapObject::MapObject(): _isLoaded(false), _hasColor(false)
{
}

MapObject::MapObject(float posX, float posY, sf::Vector2f size)
{
	_rect.setPosition(posX, posY);
	_rect.setSize(size);
	_isLoaded = true;
	_hasColor = false;
}

MapObject::MapObject(float posX, float posY, sf::Vector2f size, sf::Color color)
{
	_rect.setPosition(posX, posY);
	_rect.setSize(size);
	_rect.setFillColor(color);
	_isLoaded = true;
	_hasColor = true;
}


MapObject::~MapObject()
{
}

void MapObject::Draw(sf::RenderWindow & renderWindow)
{
	applyStyle();
	if (_hasColor) {
		renderWindow.draw(_rect);
		renderWindow.draw(_circle);
	}
}

void MapObject::SetupRect(float posX, float posY, sf::Vector2f size)
{
	_rect.setPosition(posX, posY);
	_rect.setSize(size);
	_isLoaded = true;
}

void MapObject::SetupRect(float posX, float posY, sf::Vector2f size, sf::Color color)
{
	_rect.setPosition(posX, posY);
	_rect.setSize(size);
	_isLoaded = true;
	_rect.setFillColor(color);
	_hasColor = true;
}

void MapObject::SetFillColor(sf::Color color)
{
	_rect.setFillColor(color);
	_circle.setFillColor(color);
	_hasColor = true;
}

void MapObject::SetOutline(int thickness, sf::Color Color)
{
	_circle.setOutlineThickness(10);
	_circle.setOutlineColor(sf::Color(250, 150, 100));
}

void MapObject::SetTexture(sf::Texture texture)
{
	_circle.setTexture(&texture);
	_circle.setTextureRect(sf::IntRect(10, 10, 100, 100));
}

void MapObject::applyStyle()
{
}

sf::CircleShape MapObject::getShape()
{
	return _circle;
}

void MapObject::SetupCircle(float posX, float posY, float radius, sf::Color color)
{
	_circle.setRadius(radius);
	_circle.setPosition(posX, posY);
	_circle.setFillColor(color);
	_hasColor = true;
}

sf::Rect<float> MapObject::GetBounding() const
{
	return _rect.getGlobalBounds();
}

sf::Vector2f MapObject::GetCirclePosition() const
{
	return _circle.getPosition();
}

void MapObject::Remove()
{
	_hasColor = false;
}

void MapObject::SetPosition(float posX, float posY) {
	_circle.setPosition(posX, posY);
	_rect.setPosition(posX, posY);
}

