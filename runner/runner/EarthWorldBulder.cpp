#include "EarthWorldBulder.h"



EarthWorldBulder::EarthWorldBulder()
{
}

EarthWorldBulder::EarthWorldBulder(int lvl)
{
	level = lvl;
}


EarthWorldBulder::~EarthWorldBulder()
{
}

void EarthWorldBulder::createCoints(MapObjectManager *& m)
{
	std::vector<BaseMapData> vector;
	std::cout << getFilename() + this->collectItemsName + ".txt" << "\n";
	vector = injectData(getFilename() + this->collectItemsName + ".txt");
	std::cout << vector.size() << "\n";
	for each (BaseMapData var in vector)
	{
		m->Add(factory.createCollectItem(var.getCoord()));
	}
}

std::string EarthWorldBulder::getFilename()
{
	return "Data/" +  this->path + "/" + std::to_string(level) + "/";
}

void EarthWorldBulder::createMainCollectItems(MapObjectManager *& m)
{
	std::vector<BaseMapData> vector;

	vector = injectData(getFilename() + this->mainItemsName + ".txt");
	for each (BaseMapData var in vector)
	{
		m->Add(factory.createMainCollectItem(var.getCoord()));
	}
}

void EarthWorldBulder::createBarriers(MapObjectManager *& m, sf::RenderWindow &renderWindows)
{
	std::vector<BaseMapData> vector;
	vector = injectData(getFilename() + this->barriersName + ".txt");
	for each (BaseMapData var in vector)
	{
		m->Add(factory.createBarrier(var.getCoord(), renderWindows));
	}
}
