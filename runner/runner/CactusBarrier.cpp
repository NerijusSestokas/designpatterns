#include "CactusBarrier.h"


CactusBarrier::CactusBarrier(sf::Vector2f v)
{
	SetupRect(v.x, v.y, {60, 10}, sf::Color::Green);
}

CactusBarrier::~CactusBarrier()
{
}

void CactusBarrier::addAwardsPoints()
{
	Points::getInstance()->addPoints(20);
}

void CactusBarrier::substractDemagePoints()
{
	Points::getInstance()->substractPoints(50);
}

void CactusBarrier::subsctractLifeDemagePoints(Life playerLife)
{
	playerLife.subtractHearts(0);
}
