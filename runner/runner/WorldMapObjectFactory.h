#pragma once
#include "BarrierFactory.h"
#include "CollectItemFactory.h"

class WorldMapObjectFactory
{
public:
	WorldMapObjectFactory();
	~WorldMapObjectFactory();
	virtual Barrier * createBarrier(sf::Vector2f coord, sf::RenderWindow &renderWindows) = 0;
	virtual CollectItem * createMainCollectItem(sf::Vector2f coord) = 0;
	virtual CollectItem * createCollectItem(sf::Vector2f coord) = 0;

};

