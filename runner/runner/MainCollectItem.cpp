#include "MainCollectItem.h"

MainCollectItem::MainCollectItem(sf::Vector2f coord)
{
	SetupCircle(coord.x, coord.y, 10.0f, sf::Color::Yellow);
}

MainCollectItem::~MainCollectItem()
{
}

std::string MainCollectItem::getName() {
	return "Main collect item";
}

void MainCollectItem::addPoints() {
	Points::getInstance()->addPoints(100);
}

void MainCollectItem::addLifePoins(Life playerLife)
{
	playerLife.addHearts(0);
}
