#pragma once
#include "iostream"
#include "string"
#include "Score.h"
#include <fstream>
#include <vector>
#include <sstream>
#include <algorithm>
#include <time.h>

class HighScore
{
private:
	std::string filename = "highScore.txt";
	static HighScore *instance;
	std::vector<Score> scores = std::vector<Score>();
	void getScoresFromFile();
	void addScoreToFile(Score score);
public:
	HighScore();
	~HighScore();
	static HighScore *getInstance();
	void addToHighScore(int score);
	Score getHighScore();
	const std::vector<Score> & HighScore::getAllScores()const;
	void setFilename(std::string filename);
};

