#pragma once
#include "SFML/Graphics.hpp"
#include <iostream>
class BaseMapData
{
	std::string key;
	sf::Vector2f coord;

public:
	BaseMapData();
	~BaseMapData();
	BaseMapData(std::string key, float x, float y);

	void setCoord(float x, float y);
	void setCoord(sf::Vector2f v);
	void setKey(std::string key);
	std::string getKey();
	sf::Vector2f getCoord();
};

