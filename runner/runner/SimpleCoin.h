#pragma once
#include "CollectItem.h"

class SimpleCoin : public CollectItem
{
public:
	SimpleCoin(sf::Vector2f coord, float radius);
	~SimpleCoin();
	std::string getName();
	void addPoints();
	void addLifePoins(Life playerLife);
};

