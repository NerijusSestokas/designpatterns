#pragma once
#include "Barrier.h"
#include "Points.h"
#include "Life.h"

class CactusBarrier : public Barrier
{
public:
	CactusBarrier(sf::Vector2f v);
	~CactusBarrier();

	void addAwardsPoints();
	void substractDemagePoints();
	void subsctractLifeDemagePoints(Life playerLife);
};

