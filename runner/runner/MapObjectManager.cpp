#include "MapObjectManager.h"
#include <iostream>


MapObjectManager::MapObjectManager()
{
}


MapObjectManager::~MapObjectManager()
{
}

void MapObjectManager::DrawAll(sf::RenderWindow & renderWindow)
{
	for (unsigned int i = 0; i < _mapObjects.size(); i++)
	{
		_mapObjects.at(i)->Draw(renderWindow);
	}
}

void MapObjectManager::Add(MapObject *mapObject)
{
	_mapObjects.push_back(mapObject);
}

int MapObjectManager::GetObjectCount() const
{
	return _mapObjects.size();
}

MapObject* MapObjectManager::Get(int ind)
{
	return _mapObjects.at(ind);
}

void MapObjectManager::SetLevelBoundaryX(float x)
{
	levelBoundaryX = x;
}

float MapObjectManager::GetLevelBoundaryX()
{
	return levelBoundaryX;
}
