#pragma once
#include "SFML\Graphics\Rect.hpp"

class MenuItem
{
public:
	enum MenuState 
	{
		Nothing, Play, HighScore, Levels, Exit, Momento
	};

	MenuItem();

	void SetItem(int posX, int posY, MenuState act);
	MenuState GetState();
	int GetLeft();
	int GetTop();

private:
	sf::Rect<int> rect;
	MenuState menuState;
};

