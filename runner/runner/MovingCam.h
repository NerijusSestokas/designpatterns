#pragma once
#include "CameraBehaviour.h"
class MovingCam :
	public CameraBehaviour
{
public:
	MovingCam();
	~MovingCam();

	std::string GetBehaviourName();
	void Move(sf::View & cam, float offsetX, float offsetY, sf::Vector2f playerPosition, float mapBoundaryX);

private:
	const std::string Name = "Moving";
};

