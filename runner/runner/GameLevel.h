#pragma once
#include <string>
#include <iostream>
#include "IGameLevel.h"

class GameLevel : public IGameLevel
{
protected:
	std::string world;
	int level;
	std::string filename;
public:
	GameLevel();
	GameLevel(int lvl, std::string world);
	~GameLevel();
	int getLevel();
	std::string getName();
	std::string getFilesDir();
	std::string getWorld();
	bool isNull();
};

