#include "SimpleCoin.h"

SimpleCoin::SimpleCoin(sf::Vector2f coord, float radius)
{
	SetupCircle(coord.x, coord.y, radius, sf::Color::Yellow);
}


SimpleCoin::~SimpleCoin()
{
}


std::string SimpleCoin::getName() {
	return "Simple Coin";
}

void SimpleCoin::addPoints() {
	Points::getInstance()->addPoints(10);
}

void SimpleCoin::addLifePoins(Life playerLife)
{
	playerLife.addHearts(0);
}
