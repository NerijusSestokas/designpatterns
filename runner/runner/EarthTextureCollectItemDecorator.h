#pragma once
#include "CollectItemDecorator.h"
class EarthTextureCollectItemDecorator : public CollectItemDecorator
{
public:
	EarthTextureCollectItemDecorator(CollectItem * item);
	~EarthTextureCollectItemDecorator();
	void applyStyle();
};

