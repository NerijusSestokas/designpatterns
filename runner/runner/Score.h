#pragma once
#include "string"
#include <iostream>
#include "string"
#include <sstream>

class Score
{
private:
	int score;
	std::string date;
public:
	Score(std::string date, int score);
	~Score();

	int getScore();
	void setScore(int score);
	std::string getDate();
	void setDate(std::string);
	std::string toString();
	bool operator<(Score other) const
	{
		return score < other.score;
	}
};

