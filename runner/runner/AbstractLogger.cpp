#include "AbstractLogger.h"

AbstractLoger::AbstractLoger() : _next(NULL)
{
}

void AbstractLoger::setNext(AbstractLoger * n)
{
	_next = n;
}

void AbstractLoger::add(AbstractLoger * n)
{
	if (_next)
		_next->add(n);
	else
		_next = n;
}

void AbstractLoger::handle(int level, std::string message)
{
	if (_level == level) 
	{
		logMessage(message);
	}
	if (_next != NULL)
	{
		_next->handle(level, message);
	}
}

void AbstractLoger::logMessage(std::string message)
{
}
