#pragma once
#include "CollectItemDecorator.h"
class SpaceTextureCollectItemDecorator : public CollectItemDecorator
{
public:
	SpaceTextureCollectItemDecorator(CollectItem * item);
	~SpaceTextureCollectItemDecorator();
	void applyStyle();
};

