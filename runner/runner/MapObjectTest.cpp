#include "MapObject.h"
#include <gtest\gtest.h>

TEST(MAPOBJECTTEST, RECTBOUNDING)
{
	MapObject obj;
	obj.SetupRect(5, 5, { 10, 20 });
	sf::Rect<float> rect = obj.GetBounding();
	EXPECT_EQ(rect.height, 20);
	EXPECT_EQ(rect.width, 10);
}