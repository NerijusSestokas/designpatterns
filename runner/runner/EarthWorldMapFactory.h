#pragma once
#include "WorldMapFactory.h"

class EarthWorldMapFactory
{
public:
	EarthWorldMapFactory();
	~EarthWorldMapFactory();
	Barrier * createBarrier(sf::Vector2f coord);
	CollectItem * createBlinkingCoin(sf::Vector2f coord);
	CollectItem * createCoin(sf::Vector2f coord);
};

