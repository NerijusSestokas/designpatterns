#pragma once
#include "SFML\Graphics.hpp"
#include "SFML\Graphics\Rect.hpp"
#include <iostream>
#include "GameLevelNull.h"
#include "GameLevel.h"

class Levels
{
	const sf::Vector2f space = { 50, 50 };
	const sf::Vector2f size = { 50, 50 };
public:
	Levels();
	~Levels();

	int ShowLevels(sf::RenderWindow & renderWindow);

private:
	unsigned int level;
	std::vector<sf::RectangleShape> buttons;
	std::vector<IGameLevel*> levels;

	void DrawText(std::string text, float posX, float posY, sf::RenderWindow & renderWindow);
	void LoadLevel();

	unsigned short int MouseClick(sf::RenderWindow & renderWindow);

};

