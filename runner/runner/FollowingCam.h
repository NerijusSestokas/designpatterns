#pragma once
#include "CameraBehaviour.h"

class FollowingCam :
	public CameraBehaviour
{
public:
	FollowingCam();
	~FollowingCam();

	std::string GetBehaviourName();
	void Move(sf::View & cam, float offsetX, float offsetY, sf::Vector2f playerPosition, float mapBoundaryX);

private:
	const std::string Name = "Following";
};

