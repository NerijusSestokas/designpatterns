#include "HighScore.h"
HighScore::HighScore()
{
}

HighScore::~HighScore()
{
}

HighScore * HighScore::getInstance()
{
	if (!instance) {
		instance = new HighScore();
		instance->getScoresFromFile();
	}
	return instance;
}

void HighScore::addToHighScore(int score)
{
	time_t rawtime = time(NULL);
	tm timeinfo;
	errno_t result = localtime_s(&timeinfo, &rawtime);

	std::stringstream dateStream; 
	dateStream << timeinfo.tm_year + 1900 << "-" << timeinfo.tm_mon + 1 << "-" << timeinfo.tm_mday << " "
		<< timeinfo.tm_hour << ":" << timeinfo.tm_min;
	std::string date = dateStream.str();
	Score scoreItem = Score(date, score);
	this->addScoreToFile(scoreItem);
	this->scores.push_back(scoreItem);
}

void HighScore::getScoresFromFile()
{
	this->scores.clear();
	std::ifstream scores;
	scores.open(this->filename);
	std::string date;
	int score;
	while (scores >> date >> score)
	{
		Score scoreItem = Score(date, score);
		this->scores.push_back(scoreItem);
	}
	scores.close();
}

void HighScore::addScoreToFile(Score score)
{
	std::ofstream out;
	out.open(this->filename, std::ios::app);

	out << score.toString();
	out.close();
}

Score HighScore::getHighScore()
{
	return *(std::max_element(scores.begin(), scores.end()));
}

const std::vector<Score> & HighScore::getAllScores()const
{
	return this->scores;
}

void HighScore::setFilename(std::string filename)
{
	this->filename = filename;
	this->getScoresFromFile();
}
