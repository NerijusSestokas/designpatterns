#include "SpaceWorldBulder.h"


SpaceWorldBulder::SpaceWorldBulder()
{
}

SpaceWorldBulder::SpaceWorldBulder(int lvl)
{
	level = lvl;
}


SpaceWorldBulder::~SpaceWorldBulder()
{
}



void SpaceWorldBulder::createCoints(MapObjectManager *&m)
{
	std::vector<BaseMapData> vector;
	std::cout << getFilename() + this->collectItemsName + ".txt"  << "\n";
	vector = injectData(getFilename() + this->collectItemsName + ".txt");
	for (BaseMapData var : vector)
	{
		m->Add(factory.createCollectItem(var.getCoord()));
	}
}

void SpaceWorldBulder::createMainCollectItems(MapObjectManager *&m)
{
	std::vector<BaseMapData> vector;
	vector = injectData(getFilename() + this->mainItemsName + ".txt");

	for each (BaseMapData var in vector)
	{
		m->Add(factory.createMainCollectItem(var.getCoord()));
	}
}

std::string SpaceWorldBulder::getFilename()
{
	return "Data/" + this->path + "/" + std::to_string(level) + "/";
}

void SpaceWorldBulder::createBarriers(MapObjectManager *&m, sf::RenderWindow &renderWindows)
{
	std::vector<BaseMapData> vector;
	vector = injectData(getFilename() + this->barriersName + ".txt");
	for each (BaseMapData var in vector)
	{
		factory.createBarrier(var.getCoord(), renderWindows);
	}
}