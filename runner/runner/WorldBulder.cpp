#include "WorldBulder.h"
#include "iostream"
#include <fstream>
#include <sstream>

WorldBulder::WorldBulder()
{
}


WorldBulder::~WorldBulder()
{
}

std::vector<BaseMapData> WorldBulder::injectData(std::string file)
{
	std::vector<BaseMapData> list;
	std::ifstream stream;
	stream.open(file);

	std::string key;
	float x; float y;
	while (stream >> key >> x >> y)
	{
		std::cout << "item  " + key <<  " " << x << " " << y << "\n";
		BaseMapData item = BaseMapData(key, x, y);
		list.push_back(item);
	}
	stream.close();

	return list;
}
