#pragma once
#include "CollectItem.h"

class MainCollectItem : public CollectItem
{
public:
	MainCollectItem(sf::Vector2f coord);
	~MainCollectItem();

	std::string getName();
	void addPoints();
	void addLifePoins(Life playerLife);
};

