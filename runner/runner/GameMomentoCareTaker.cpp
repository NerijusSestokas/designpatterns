#include "GameMomentoCareTaker.h"



GameMomentoCareTaker::GameMomentoCareTaker()
{
}


GameMomentoCareTaker::~GameMomentoCareTaker()
{
}

void GameMomentoCareTaker::addGameMomento(GameMomento momento)
{
	if (momentoList.size() == 0) {
		momento.setIdentifier();
		momentoList.push_back(momento);
	}
}

GameMomento GameMomentoCareTaker::getMomento(int index)
{
	return momentoList.at(index);
}

GameMomento GameMomentoCareTaker::getMomento()
{
	GameMomento mom = momentoList.back();
	momentoList.pop_back();
	return mom;
}

std::vector<GameMomento> GameMomentoCareTaker::getList()
{
	return momentoList;
}
