#pragma once
#include "SFML\Graphics.hpp"
#include <iostream>
class MapObject
{
public:
	MapObject();
	MapObject(float posX, float posY, sf::Vector2f size);
	MapObject(float posX, float posY, sf::Vector2f size, sf::Color color);
	virtual ~MapObject();

	virtual void Draw(sf::RenderWindow &renderWindow);

	virtual void SetupRect(float posX, float posY, sf::Vector2f size);
	virtual void SetupRect(float posX, float posY, sf::Vector2f size, sf::Color color);
	virtual void SetFillColor(sf::Color color);
	virtual void SetOutline(int thickness, sf::Color Color);
	virtual void SetTexture(sf::Texture texture);
	virtual void applyStyle();
	virtual sf::CircleShape getShape();

	virtual void SetupCircle(float posX, float posY, float radius, sf::Color color);

	virtual sf::Rect<float> GetBounding() const;
	virtual sf::Vector2f MapObject::GetCirclePosition() const;

	virtual void Remove();
	virtual void SetPosition(float posX, float posY);

private:
	sf::CircleShape _circle;
	sf::RectangleShape _rect;
	bool _isLoaded;
	bool _hasColor;
	void Draw(sf::Vector2f coord, sf::RenderWindow & renderWindow);
};

