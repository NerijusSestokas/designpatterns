#include "Points.h"
#include <gtest\gtest.h>

TEST(POINTSTESTS, PointsInnicialize) {
	EXPECT_EQ(0, Points::getInstance()->getPoints());
}

TEST(POINTSTESTS, PointsAdd) {
	Points::getInstance()->addPoints(5);
	EXPECT_EQ(5, Points::getInstance()->getPoints());
}

TEST(POINTSTESTS, PointsSubstract) {
	Points::getInstance()->substractPoints(2);
	EXPECT_EQ(3, Points::getInstance()->getPoints());
}

TEST(POINTSTESTS, PointsSubstractToZero) {
	Points::getInstance()->substractPoints(10);
	EXPECT_EQ(0, Points::getInstance()->getPoints());
}

TEST(POINTSTESTS, PointsNulify) {
	Points::getInstance()->addPoints(152);
	Points::getInstance()->nulifyPoints();
	EXPECT_EQ(0, Points::getInstance()->getPoints());
}