#pragma once
#include "CollectItem.h"

class LifeCollectItem : public CollectItem
{
public:
	LifeCollectItem(sf::Vector2f coord);
	~LifeCollectItem();

	std::string getName();
	void addPoints();
	void addLifePoins(Life playerLife);
};

