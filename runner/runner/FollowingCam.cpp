#include "FollowingCam.h"



FollowingCam::FollowingCam()
{
}


FollowingCam::~FollowingCam()
{
}

std::string FollowingCam::GetBehaviourName()
{
	return Name;
}

void FollowingCam::Move(sf::View & cam, float offsetX, float offsetY, sf::Vector2f playerPosition, float mapBoundaryX)
{
	float xc = cam.getCenter().x;
	if (xc <= cam.getSize().x / 2 && playerPosition.x < xc)
	{
		cam.setCenter(cam.getSize().x / 2.0f, cam.getSize().y / 2.0f);
	}
	else if (cam.getCenter().x < mapBoundaryX - (cam.getSize().x / 2))
	{
		cam.move(offsetX, offsetY);
	}
}