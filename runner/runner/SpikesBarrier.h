#pragma once
#include "Barrier.h"
#include "Life.h"
#include "Points.h"

class SpikesBarrier : public Barrier
{
public:
	SpikesBarrier(sf::Vector2f v);
	~SpikesBarrier();

	void addAwardsPoints();
	void substractDemagePoints();
	void subsctractLifeDemagePoints(Life playerLife);
};

