#include "Points.h"

Points::Points(){}

Points::~Points(){}

void Points::addPoints(int points)
{
	this->points += points;
}

void Points::substractPoints(int points)
{
	this->points -= points;
	if (this->points < 0) {
		this->points = 0;
	}
}

void Points::nulifyPoints()
{
	this->points = 0;
}

int Points::getPoints()
{
	return this->points;
}

void Points::setPoints(int points)
{
	this->points = points;
}

Points *Points::getInstance()
{
	if (!instance) {
 		instance = new Points();
	}
	return instance;
}