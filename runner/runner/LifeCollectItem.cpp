#include "LifeCollectItem.h"



LifeCollectItem::LifeCollectItem(sf::Vector2f coord)
{
	SetupCircle(coord.x, coord.y, 10.0f, sf::Color::Yellow);
}


LifeCollectItem::~LifeCollectItem()
{
}


std::string LifeCollectItem::getName() {
	return "Life item";
}

void LifeCollectItem::addPoints() {
	Points::getInstance()->addPoints(0);
}

void LifeCollectItem::addLifePoins(Life playerLife)
{
	playerLife.addHearts(1);
}

void MapObject::Draw(sf::Vector2f coord, sf::RenderWindow & renderWindow)
{
	SetupCircle(coord.x, coord.y, 10.0f, sf::Color::Red);
	this->Draw(renderWindow);
}