#include "Player.h"

Player::Player() : _runSpeed(0), _maxSpeed(1000.0f), _jump(0), _maxJump(1000.0f), _jumpBool(false), _floor(false)
{
	Load("Character/cat.png");
	assert(IsLoaded());
	clkAnim.restart();
	textureRect.height = 256;
	textureRect.width = 256;
	textureRect.top = 256;
	textureRect.left = 0;
	this->SetScale(0.5f, 0.5f);
	GetSprite().setTextureRect(textureRect);
	cam.SetBehavior(new FollowingCam());
//	cam.SetBehavior(new MovingCam());
	cam.SetCameraSize(800.0f, 600.0f);
}


Player::~Player()
{
}

void Player::Update(float elapsedTime)
{
	Move();
	CheckFloor();
	CheckCoins();
	clkElapsed = elapsedTime;

	if (!_jumpBool && !_floor)
	{
		_jump += _maxJump * 0.009f;
	}
	else if (!_jumpBool && _floor)
	{
		_jump = 0;
	}
	else if (_jumpBool)
	{
		_jump = -_maxJump;
	}

	// Stop jump after some time
	if (_jumpBool && clkJump.getElapsedTime().asSeconds() >= 0.5f)
	{
		_jumpBool = false;
	} 

	PlayTexture();
	GetSprite().move(_runSpeed * elapsedTime, _jump * elapsedTime);
}

void Player::Draw(sf::RenderWindow & renderWindow)
{
	VisibleGameObject::Draw(renderWindow);

	if (cam.GetBehaviourName() == "Following") 
	{
		cam.Move(_runSpeed * clkElapsed, 0, this->GetPosition(), map->GetLevelBoundaryX());
	}
	else if (cam.GetBehaviourName() == "Moving")
	{
		cam.Move(100 * clkElapsed, 0, this->GetPosition(), map->GetLevelBoundaryX());
	}
	cam.SetView(renderWindow);

	if (this->GetPosition().x == 160) {
		cam.ResetView();
		cam.SetView(renderWindow);
	}
}

float Player::GetSpeed() const
{
	return _runSpeed;
}

void Player::SetMap(MapObjectManager* map)
{
	this->map = map;
}

void Player::Move()
{
	if (sf::Keyboard::isKeyPressed(sf::Keyboard::Left))
	{
		_runSpeed = -_maxSpeed;
		this->FlipSpriteLeft();
	}
	else if (sf::Keyboard::isKeyPressed(sf::Keyboard::Right))
	{
		_runSpeed = _maxSpeed;
		this->FlipSpriteRight();
	}
	else
	{
		_runSpeed = 0;
	}

	if (sf::Keyboard::isKeyPressed(sf::Keyboard::Up) && clkJump.getElapsedTime().asSeconds() > 1.2f)
	{
		_jumpBool = true;
		clkJump.restart();
	}

}

void Player::texturePosition(unsigned short int anim)
{
	
	switch (anim)
	{
		case 0:			// Idle
		{
			textureRect.top = 256;
			if (clkAnim.getElapsedTime().asSeconds() <= 0.8f)
			{
				textureRect.left = 0;
			}
			else if (clkAnim.getElapsedTime().asSeconds() > 0.8f && clkAnim.getElapsedTime().asSeconds() < 1.0f)
			{
				textureRect.left = 256;
			}
			else 
			{
				clkAnim.restart();
			}
			break;
		}
		case 1:			// Run
		{
			textureRect.top = 0;
			if (clkAnim.getElapsedTime().asSeconds() > 0.05f)
			{
				if (textureRect.left == 4608)
					textureRect.left = 0;
				else
					textureRect.left += 256;

				clkAnim.restart();
			}
			break;
		}
		case 2:			// Jump
		{
			textureRect.top = 512;
			if (clkAnim.getElapsedTime().asSeconds() > 0.5f)
			{
				if (textureRect.left >= 1792)
					textureRect.left = 0;
				else
					textureRect.left += 256;

				clkAnim.restart();
			}
			if (textureRect.left >= 1792)
				textureRect.left = 0;
			break;
		}
	}
	GetSprite().setTextureRect(textureRect);
}

void Player::CheckFloor()
{
	for (int i = 0; i < map->GetObjectCount(); i++)
	{
		if (map->Get(i)->GetBounding().intersects(this->GetSprite().getGlobalBounds()))
		{
			_floor = true;
			break;
		}
		else
		{
			_floor = false;
		}
	}
}

void Player::PlayTexture()
{
	if (!_floor)
	{
		texturePosition(2);
	}
	else if (_runSpeed != 0)
	{
		texturePosition(1);
	}
	else if (_floor && _runSpeed == 0)
	{
		texturePosition(0);
	}
}

sf::Vector2f Player::GetPlayerPosition()
{
	sf::Vector2f vector;
	vector.x = GetSprite().getPosition().x;
	vector.x = GetSprite().getPosition().y;
	return vector;
}

void Player::CheckCoins()
{
//	std::cout << "PLAYER:  " << this->GetSprite().getPosition().x << "\n";
	for (int i = 0; i < map->GetObjectCount(); i++)
	{
//		std::cout << "COIN:  " << i << " " << map->Get(i)->GetCirclePosition().x << "\n";
		if ((map->Get(i)->GetCirclePosition().x < this->GetSprite().getPosition().x + 10
			&& map->Get(i)->GetCirclePosition().x > this->GetSprite().getPosition().x - 10))
		{
//			std::cout << "TOUCHING\n";
			break;
		}
	}
}
