#include "SpaceWorldMapFactory.h"



SpaceWorldMapFactory::SpaceWorldMapFactory()
{
}


SpaceWorldMapFactory::~SpaceWorldMapFactory()
{
}

Barrier * SpaceWorldMapFactory::createBarrier(sf::Vector2f coord)
{
	return  BarrierFactory::getInstance()->createBarrier("rectangle", coord);
}

CollectItem * SpaceWorldMapFactory::createBlinkingCoin(sf::Vector2f coord)
{
	return CollectItemFactory::getInstance()->CreateItem("small_blinking", coord);
}

CollectItem * SpaceWorldMapFactory::createCoin(sf::Vector2f coord)
{
	return CollectItemFactory::getInstance()->CreateItem("small", coord);
}

